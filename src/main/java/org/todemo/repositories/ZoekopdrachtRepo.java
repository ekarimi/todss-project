package org.todemo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.todemo.model.Zoekopdracht;

/**
 * Created by Trajche on 14-Jun-17.
 */
public interface ZoekopdrachtRepo extends CrudRepository<Zoekopdracht, Integer> {
}
