package org.todemo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.todemo.model.Prospect;

/**
 * Created by Trajche on 14-Jun-17.
 */
public interface ProspectRepo extends CrudRepository<Prospect, Integer> {
}
