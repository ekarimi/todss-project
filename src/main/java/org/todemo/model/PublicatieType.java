package org.todemo.model;

/**
 * Created by e-kar on 12-6-2017.
 */
public enum PublicatieType {
        ARTIKEL, BOEK, TWEETS, LINKEDIN_POSTS;
}
