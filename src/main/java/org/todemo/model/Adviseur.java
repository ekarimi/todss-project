package org.todemo.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

/**
 * Created by e-kar on 6/11/17.
 */
@Entity

public class Adviseur {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private int id;
    private String voornaam, achternaam, email, website;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Publicatie> publicatieList;

    public Adviseur() {
    }

    public Adviseur(String voornaam, String achternaam, String email, String website) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.email = email;
        this.website = website;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<Publicatie> getPublicatieList() {
        return publicatieList;
    }

    public void setPublicatieList(List<Publicatie> publicatieList) {
        this.publicatieList = publicatieList;
    }

    public void addPublicatie(Publicatie publicatie) {
        publicatieList.add(publicatie);
    }
}
