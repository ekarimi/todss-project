package org.todemo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.todemo.model.Adviseur;

/**
 * Created by Trajche on 14-Jun-17.
 */
public interface AdviseurRepo extends CrudRepository<Adviseur, Integer> {
}