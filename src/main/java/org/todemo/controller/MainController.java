package org.todemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.todemo.model.Adviseur;
import org.todemo.model.Prospect;
import org.todemo.model.Zoekopdracht;
import org.todemo.repositories.AanvraagRepo;
import org.todemo.repositories.AdviseurRepo;
import org.todemo.repositories.ProspectRepo;
import org.todemo.repositories.PublicatieRepo;

import java.util.Date;

/**
 * Created by Trajche on 11-Jun-17.
 */
// Deze klasse wordt alleen gebruikt om java objecten op te slaan in de DB. Eerst objecte zelf en
// dan relaties. Voer deze methodes uit op een volgorde, want anders kan het zijn dat duplicated data
// naar de database gaat.

// Voordat je deze methodes uitvoert, moet je je DB schema dropen en opnieuwe aanmaken (zodat die leeg is en tabellen heeft)

// Deze klasse is NOG NIET af. Kijk naar voorbeelden.

@RestController
public class MainController {

    @Autowired
    private DataService dataService;  //Om aan test objecten te kunnen komen

    @Autowired
    private AdviseurRepo adviseurRepo;

    @Autowired
    private PublicatieRepo publicatieRepo;

    @Autowired
    private AanvraagRepo aanvraagRepo;

    @Autowired
    private ProspectRepo prospectRepo;


    // Voer de onderstaande methodes uit in volgorde

    // 1. Deze methode zorgt ervoor dat de data ZONDER relaties worden opgeslagen in de database

    @RequestMapping(value = "/storeAllData")
    public void persistAllData() {
        adviseurRepo.save(dataService.getTestAdviseurs());
        publicatieRepo.save(dataService.getTestPublicaties());

        // ....
    }

    // 2. Deze methodes zorgen ervoor dat de data MET relaties worden opgeslagen in de database

    @RequestMapping(value = "/assignPublicatiesToAdviseurs")
    public void addPublicatiesToAdviseur() {
        for (Adviseur adviseur : adviseurRepo.findAll()) {
            // Add publicatie met id 1 uit db aan alle adviseurs
            adviseur.addPublicatie(publicatieRepo.findOne(1));
        }
    }

    @RequestMapping(value = "/assignAanvragenToProspect")
    public void addAanvragenToProspect() {
        for (Prospect prospect : prospectRepo.findAll()) {
            // Add een zoekopdracht en een aanvraag (uit db) aan alle prospects
            prospect.addAanvraag(aanvraagRepo.findOne(1));
            prospect.addZoekOpdrachten(new Zoekopdracht("Test zoekterm", new Date()));

        }
    }

    // .....


}
