package org.todemo.controller;

import org.springframework.stereotype.Service;
import org.todemo.model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Trajche on 11-Jun-17.
 */

@Service
public class DataService {

    public void CVReader(){
        File cv = new File("C:/");
        FileInputStream fis = null;

        try{
            fis = new FileInputStream(cv);
            int content;
            while((content = fis.read()) !=1){
                System.out.print((char) content);
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try{
                if (fis != null)
                    fis.close();
            } catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }

    public Aanvraag getTestAanvragen() {
        return new Aanvraag(new Date(), AanvraagType.CV);
        //...
    }

    public List<Adviseur> getTestAdviseurs() {
        List<Adviseur> adviseurs = new ArrayList<>();
        adviseurs.add(new Adviseur("Jan", "van Molen", "jan@aol.com", "http://jan.com"));
        adviseurs.add(new Adviseur("Peter", "Roos", "peter@aol.com", "http://peter.com"));
        adviseurs.add(new Adviseur("Bill", "Coughran", "billC12@genius.com", "http://BCgenius.com"));

        return adviseurs;
    }
        //er moet een link zitten tussen de publicatis en adviseurs, want iedere publicatie is door een adviseur geschreven
    public List<Publicatie> getTestPublicaties() {
        List<Publicatie> publicaties = new ArrayList<>();
        publicaties.add(new Publicatie("How to cook Mantoo", "http://chefelias.com"));
        publicaties.add(new Publicatie("Innovation Enterprise", "The innovation enterprise offers articles about innovation and digitalization. The articles can be categorised by certain channels, such as innovation, digital or strategy, or for certain c-suite specialists, such as for a CTO, CDO or CSO, which allows readers to find interesting articles in a very convenient way. Additionally, the website provides different content types like interviews, eBooks and webinars.",
                "theinnovationenterprise.com/", "bron", PublicatieType.ARTIKEL));
        publicaties.add(new Publicatie("Collective Genius", "Few companies have the resources of Google at their disposal, but most of them can relate to Coughran’s fundamental challenge. In 2005 we joined together to study exceptional leaders of innovation—how they think, what they do, and who they are. We found them across the globe—in Silicon Valley, Europe, the United Arab Emirates, India, and Korea. And we explored businesses as varied as filmmaking, e-commerce, auto manufacturing, professional services, and luxury goods. We didn’t think the world needed more research on leaders or on innovation. Rather, we wanted to study a topic much less understood: the role of the leader in creating a more innovative organization.",
                "https://hbr.org/2014/06/collective-genius", "bron", PublicatieType.ARTIKEL));

        return publicaties;
    }

    //  .....
}
