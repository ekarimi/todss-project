package org.todemo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by e-kar on 12-6-2017.
 */
@Entity

public class Zoekopdracht {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private int id;
    private String term;
    private Date datum;

    public Zoekopdracht() {
    }

    public Zoekopdracht(String term, Date datum) {
        this.term = term;
        this.datum = datum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }
}
