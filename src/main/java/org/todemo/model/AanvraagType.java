package org.todemo.model;

/**
 * Created by e-kar on 12-6-2017.
 */
public enum AanvraagType {
    CV, STATISTIC, REPORT;
}
