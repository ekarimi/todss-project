package org.todemo.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by e-kar on 11-6-2017.
 */
@Entity
public class Aanvraag {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Temporal(TemporalType.TIMESTAMP)

    private Date datum;
    @Enumerated(EnumType.STRING)
    private AanvraagType type;
    @OneToOne(fetch = FetchType.EAGER)

    private Publicatie publicatie;

    public Aanvraag(Date datum, AanvraagType type) {
        this.datum = datum;
        this.type = type;
    }

    public Aanvraag() {
    }

    public Publicatie getPublicatie() {
        return publicatie;
    }

    public void setPublicatie(Publicatie publicatie) {
        this.publicatie = publicatie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public AanvraagType getType() {
        return type;
    }

    public void setType(AanvraagType type) {
        this.type = type;
    }

}
