package org.todemo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.todemo.model.Aanvraag;

/**
 * Created by Trajche on 14-Jun-17.
 */
public interface AanvraagRepo extends CrudRepository<Aanvraag, Integer> {
}
