package org.todemo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.todemo.model.Publicatie;

/**
 * Created by Trajche on 14-Jun-17.
 */
public interface PublicatieRepo extends CrudRepository<Publicatie, Integer> {
}
