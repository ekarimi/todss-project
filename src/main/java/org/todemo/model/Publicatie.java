package org.todemo.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by e-kar on 6/11/17.
 */
@Entity

public class Publicatie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private int id;
    private String titel, inhoud, url, bron;
    @Enumerated(EnumType.STRING)

    private PublicatieType type;

    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Aanvraag> aanvragList = new ArrayList<>();

    public Publicatie(String titel, String inhoud, String url, String bron, PublicatieType type ) {
        this.titel = titel;
        this.inhoud = inhoud;
        this.url = url;
        this.bron = bron;
        this.type = type;
    }

    public Publicatie(String titel, String url) {
        this.titel = titel;
        this.url = url;
    }

    public Publicatie() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getInhoud() {
        return inhoud;
    }

    public void setInhoud(String inhoud) {
        this.inhoud = inhoud;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBron() {
        return bron;
    }

    public void setBron(String bron) {
        this.bron = bron;
    }

    public PublicatieType getType() {
        return type;
    }

    public void setType(PublicatieType type) {
        this.type = type;
    }


    public List<Aanvraag> getAanvragList() {
        return aanvragList;
    }

    public void setAanvragList(List<Aanvraag> aanvragList) {
        this.aanvragList = aanvragList;
    }

    public void addAanvraag(Aanvraag aanvraag) {
        aanvragList.add(aanvraag);
    }

}
