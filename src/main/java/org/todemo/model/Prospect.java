package org.todemo.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

/**
 * Created by e-kar on 11-6-2017.
 */
@Entity

public class Prospect {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private int id;
    private String email, name;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Zoekopdracht> zoekopdrachten;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Aanvraag> aanvragen;

    public Prospect() {
    }

    public Prospect(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Zoekopdracht> getZoekopdrachten() {
        return zoekopdrachten;
    }

    public void setZoekopdrachten(List<Zoekopdracht> zoekopdrachten) {
        this.zoekopdrachten = zoekopdrachten;
    }

    public List<Aanvraag> getAanvragen() {
        return aanvragen;
    }

    public void setAanvragen(List<Aanvraag> aanvragen) {
        this.aanvragen = aanvragen;
    }

    public void addAanvraag(Aanvraag aanvraag) {
        aanvragen.add(aanvraag);
    }

    public void addZoekOpdrachten(Zoekopdracht zoekopdracht) {
        zoekopdrachten.add(zoekopdracht);
    }
}
